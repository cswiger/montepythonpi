#!/usr/bin/env python3
#
#  https://en.wikipedia.org/wiki/Buffon%27s_needle_problem
#
# length of toothpick - length = 10
# distance between lines - distance = 20
#
# pi = 2 * (length/distance) * (total throws / number that cross a line)

from random import random
import math

# set boundaries and lines
x1 = 0.	  # end
x2 = 20.   # line to cross or not
x3 = 40.   # line to cross or not
x4 = 60.   # end

# initialize crossing counter
crosses = 0

for ndx in range(5000000):
   # pick a value from 10 thru 50, that is 10 below one line at x=20 and 10 over the other at x=40
   x = random()*40.+10
   # random angle of rotation
   ang = random()*360.
   # now the end is 10 units from the begin along the x-axis by the angle
   xend = float(x)+10.*math.cos(ang/180.*math.pi)
   # if that crosses x=20 or x=40 count it
   if ((x <= x2 and xend >= x2) or (x >= x2 and xend <= x2) or (x <= x3 and xend >= x3) or (x >= x3 and xend <= x3)):
      crosses += 1

# print results
res = 5000000./float(crosses)
print("Monte carlo pi:", res)
print("Error :", abs(math.pi-res))

