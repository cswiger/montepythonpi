#!/usr/bin/env python

from random import randint
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg

"""
 game of non-transitive dice

 https://en.wikipedia.org/wiki/Nontransitive_dice
 https://singingbanana.com/dice/article.htm

 player lets victim pick one of the 3 die, then player takes the remaining one that
 is likely to beat the victim's die at least 7 out of 12 times

 play n games, where each game is a bet of 1 credit
 both start with a pot of 100 credits, winner gets 1, loser loses one

 dice per https://singingbanana.com/dice/article.htm
 da beats db 7/12,  db beats dc 7/12 and dc beats da 21 out of 36 times
"""

da=[3,3,3,3,3,6]
db=[2,2,2,5,5,5]
dc=[1,4,4,4,4,4]

"""
da plays db:
da -- 3  5/6  --  db 2   3/6    =  15/36   da wins
                  db 5   3/6    =  15/36   da loses

      6  1/6  --  db 2   3/6    =  3/36   da wins
                  db 5   3/6    =  3/36   da wins    -- da wins over db 21/36 = 7/12 times


db plays dc:
db -- 2  3/6  --  dc 1   1/6    =  3/36    db wins
                  dc 4   5/6    =  15/36   db loses

      5  3/6  --  dc 1   1/6	=  3/36    db wins
                  dc 4   5/6    =  15/36   db wins   --  db wins over dc 21/36 =  7/12 times

dc plays da:
dc -- 1  1/6  --  da  3   5/6   =  5/36   dc loses 
                  da  6   1/6   =  1/36   dc loses

      4  5/6  --  da  3   5/6   =  25/36  dc wins
                  da  6   1/6   =  5/36   dc loses    -- dc wins over da  25/36 times, or 8.222/12 times
"""


# array of the dice
dar = [da,db,dc]

# victim picks a die
vd = randint(0,2)

# player knows the winning die is the previous one modulo 3
pd = (vd - 1) % 3

# start with 100 credits each
ppot = 100
vpot = 100

# play n games
n = 1000

# keep track of outcomes
ppothist = []
vpothist = []

for ndx in range(n):
  # roll the players die
  rp = dar[pd][randint(0,5)]
  # and the victims die
  rv = dar[vd][randint(0,5)]
  # score if player beats victim
  if (rp > rv):
    ppot += 1
    vpot -= 1
  # or victim wins
  if (rv > rp):
    ppot -= 1
    vpot += 1
  # record outcome
  ppothist.append(ppot)
  vpothist.append(vpot)
  # stop the games if either busts
  if ((vpot == 0) or (ppot == 0)): break


# graph the progress using pyqtgraph

win = pg.GraphicsWindow(title="Non-Transitive Dice")
win.resize(1000,600)

# Enable antialiasing for prettier plots
pg.setConfigOptions(antialias=True)

p6 = win.addPlot(title="Non-Transitive Dice")
p6.addLegend()
# yellow is the victim, blue the player
curve1 = p6.plot(pen='y',name="victims pot")    
curve2 = p6.plot(pen='b',name="players pot")
curve1.setData(vpothist)
curve2.setData(ppothist)

QtGui.QApplication.instance().exec_()


