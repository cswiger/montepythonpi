#!/usr/bin/env python
# repeatedly run a birthday check on varying population sizes for matching birthdays using many trials and report results
# should get better than even odds for populations of 23 or more, marked with an '*' in the report
from random import randint
import numpy as np
import sys

# size of population to start with
popsize=popsize_start=10
# size to end with, inclusive
popsize_end=30
# number of trials for each size population
trials=1000

# keeps track of matching birthdays for each population size 
# prefill counts in with zeros to access with index
counts=np.zeros(popsize_end-popsize_start+1)

# go from population start size to end size
while(popsize <= popsize_end):
  # number of trials for each population size
  for trial in range(trials):
    # re-initialize array of birthdays each run
    person=[]
    found=False

    # generate integer day of year birthdays from 1 to 365
    for ndx in range(popsize):
      person.append(randint(1,365))

    # now find matching birthdays
    for ndx in range(popsize):
      search = [i for i, e in enumerate(person) if e == person[ndx]]
      if (len(search) > 1):
        # found two that match
        found=True
        # no need to keep searching then
        break
    if (found==True):
      # score it for this trial
      counts[popsize-popsize_start] += 1 

  # report on this population size
  sys.stdout.write("Population size: "+str(popsize)+" Found "+str(int(counts[popsize-popsize_start]))+" matches out of "+str(trials)+" trials")
  # mark greater than even odds
  if (counts[popsize-popsize_start] > 0.5 * trials):
    sys.stdout.write(" *\n")
  else:
    sys.stdout.write("\n")
  # do the next size population 
  popsize+=1




