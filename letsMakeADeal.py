#!/usr/bin/env python
"""
Simple simulation of Lets Make a Deal where Monty Hall has 3 doors
Behind one of the doors is a Brand New Car!!
and the other two have goats. 
The Player picks a door at random, then Monty shows which one of the
other doors has a goat. 
Is it just as good for the player to stick with his choice or
switch to the other unopened door?

https://en.wikipedia.org/wiki/Monty_Hall_problem
Odds of winning should be 1/3 when sticking, and 2/3 when switching

Do two runs of games with the different strategies 

"""

from random import randint

# number of games to run for each strategy
n = 1000
wins = 0

# Strategy 1 - sick with original pick, 1/3 chance of winning
for ndx in range(n):
  # door with the car
  cdoor = randint(0,2)
  # player picks door
  pdoor = randint(0,2)
  # does not matter what Monty shows, player is sticking with his first pick
  if ( pdoor == cdoor ):
    wins += 1

print("Sticking Strategy wins out of %s: %s" % (n,wins))

# Strategy 2 - after Monty reveals another door that has a goat, player switches 

wins = 0

for ndx in range(n):
  # door with the car
  cdoor = randint(0,2)
  # player picks door
  pdoor = randint(0,2)
  # Monty picks one of the goat doors and opens it to show the player
  # keep picking random until it is NOT the door with the car or players door
  mdoor = randint(0,2)
  while ( (mdoor == cdoor) or (mdoor == pdoor) ):
    mdoor = randint(0,2)
  # Now player switches to the one that is not pdoor or mdoor
  # there's got to be a better way to do this
  npdoor = randint(0,2)
  while ( (npdoor == pdoor) or (npdoor == mdoor)):
    npdoor = randint(0,2)
  # check for win
  if (npdoor == cdoor):
    wins += 1

print("Switching Strategy wins out of %s: %s" % (n,wins))



