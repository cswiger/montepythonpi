# montepythonpi

Experiments with numerical method Monty Carlo

montePythonPi.py - Calculate Pi with random points in a circle and square

kelly.py - testing Kelly Criterion  { https://en.wikipedia.org/wiki/Kelly_criterion } or how much of a bankroll to bet on given odds and amount of payoff. This instance tests the random roll of a die, 1/6 chance of winning a 10x payout, betting the Kelly formula each time. Draws chart of the state of the bankroll (yellow), the amount bet eah time (blue), and prints out the final result including bankroll value, numbers of wins out of the number of trials and what it should have been, 1/6 or 0.16666.

birthdays.py - run many trials on varying population sizes from 10 thru 30, looking for the number of times two people in the population have the same birthday. Should get better than even odds for populations of 23 or more. Can set the sizes to test, and how many trials to run. 

letsMakeADeal.py - try out the strategy of switching doors -vs- not switching when another door is opened after picking one of three. 

nontransdice.py - victim picks one of three dice. No matter which one is picked one of the other two can beat it! Uses pyqtgraph to plot game progress.

buffonsNeedle.py - calculate Pi from randomly dropping toothpicks on a lined paper, using how many cross a line out of the total number of drops.

