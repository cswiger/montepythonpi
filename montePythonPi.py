#!/usr/bin/env python
# Calculate Pi using random points in a square and a circle
# r=1 unit circle has area:  Pi*r^2 = Pi
# square has area:  (2r)^2 = 4
# ratio of circle to square is then Pi/4

import sys
import numpy as np

# random point function, returns a 2d point in the square -1 to 1 with uniform distribution
def bullet():
   return np.random.rand()*2-1, np.random.rand()*2-1

# do 10 Million iterations
square=10000000
circle=0
for it in range(square):
   # get a point in the square
   (x,y) = bullet()
   # is it also in the unit circle?
   if (np.sqrt(x**2+y**2)<=1):
      circle+=1

sys.stdout.write("Monte Python Pi: ")
print(4.*float(circle)/float(square)) 
sys.stdout.write("Error: ")
print(abs(4.*float(circle)/float(square)-np.pi))

