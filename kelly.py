#!/usr/bin/env python
from random import random
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import sys

# how many rounds to play
rounds = 200

# odds of winning  - 1 out of 6
p=1./6.
q=1-p    # so odds of losing

# But payoff is 10x
b=10.

# Kelly - how much of bankroll to bet
# https://en.wikipedia.org/wiki/Kelly_criterion
f=(b*p-q)/b  # is 0.083333 here

# start with $100
bankroll = 100.

# keep track of bankroll state
state=[bankroll]
bets=[]
wins=0

# roll a die
def roll():
  if (random() < 1./6.):
    return 1
  else:
    return 0

for ndx in range(rounds):
  bet = f*bankroll
  bets.append(bet)
  if (roll() == 1):
     # we have a winner!
     bankroll += b*bet
     wins += 1
  else:
     # Ahhhh
     bankroll -= bet
  state.append(bankroll)

sys.stdout.write("final bankroll: ")
print(round(bankroll,2))
sys.stdout.write("Number of wins: ")
sys.stdout.write(str(wins))
sys.stdout.write(" out of ")
sys.stdout.write(str(rounds))
sys.stdout.write(" rounds, or ")
print(str(float(wins)/float(rounds)))
sys.stdout.write("Should be ")
print(str(1./6.))

win = pg.GraphicsWindow(title="Kelly Criterion")
win.resize(1000,600)
pg.setConfigOptions(antialias=True)
p6 = win.addPlot(title="Bankroll state")
curve = p6.plot(pen='y')   
curve.setData(state)
curve2 = p6.plot(pen='b')
curve2.setData(bets)
QtGui.QApplication.instance().exec_()


